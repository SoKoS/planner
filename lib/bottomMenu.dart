import 'package:flutter/material.dart';
import 'package:vector_math/vector_math_64.dart' as math;

class BottomMenu extends StatefulWidget {
  final void Function() onChangePlan;
  final void Function() resetScale;
  final void Function() addElement;

  const BottomMenu(
      {Key key,
      @required this.resetScale,
      @required this.addElement,
      @required this.onChangePlan})
      : super(key: key);

  @override
  _BottomMenuState createState() => _BottomMenuState();
}

class _BottomMenuState extends State<BottomMenu>
    with SingleTickerProviderStateMixin {
  double isClosed = 1;
  var icon;

  Animation<double> animation;
  AnimationController controller;

  @override
  void initState() {
    super.initState();
    controller = AnimationController(
        duration: const Duration(milliseconds: 100), vsync: this);
    animation = Tween<double>(begin: -100, end: 0).animate(controller)
      ..addListener(() {
        setState(() {});
      });
  }

  @override
  Widget build(BuildContext context) {
    return Transform(
      transform: Matrix4.translation(math.Vector3(animation.value, 0, 0)),
      child: Container(
        color: Colors.white12,
        child: Row(
          children: <Widget>[
            // SizedBox(
            //   height: 60,
            //   width: 50,
            //   child: FlatButton(
            //     padding: EdgeInsets.all(0),
            //     onPressed: () {
            //       //TODO
            //     },
            //     child: Icon(Icons.undo),
            //   ),
            // ),
            // SizedBox(
            //   height: 60,
            //   width: 50,
            //   child: FlatButton(
            //     padding: EdgeInsets.all(0),
            //     onPressed: () {
            //       //TODO
            //     },
            //     child: Icon(Icons.redo),
            //   ),
            // ),
            SizedBox(
              height: 60,
              width: 50,
              child: FlatButton(
                padding: EdgeInsets.all(0),
                onPressed: () {
                  widget.onChangePlan();
                },
                child: Icon(Icons.map),
              ),
            ),
            SizedBox(
              height: 60,
              width: 50,
              child: FlatButton(
                padding: EdgeInsets.all(0),
                onPressed: () {
                  widget.resetScale();
                },
                child: Icon(Icons.zoom_in),
              ),
            ),
            SizedBox(
              height: 60,
              width: 50,
              child: FlatButton(
                padding: EdgeInsets.all(0),
                onPressed: () {
                  widget.addElement();
                },
                child: Icon(Icons.add),
              ),
            ),
            SizedBox(
              height: 60,
              width: 50,
              child: FlatButton(
                padding: EdgeInsets.all(0),
                onPressed: () {
                  if (isClosed == 1) {
                    icon = Icons.arrow_back;
                    isClosed = 0;
                    controller.forward();
                  } else {
                    icon = Icons.arrow_forward;
                    isClosed = 1;
                    controller.animateBack(0);
                  }
                  setState(() {});
                },
                child: Icon(icon ?? Icons.arrow_forward),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
