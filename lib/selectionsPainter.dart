import 'package:flutter/material.dart';
import 'package:planner/globals.dart' as globals;

class SelectionsPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint()
      ..style = PaintingStyle.stroke
      ..color = Colors.white
      ..strokeWidth = 1;

    canvas.drawRect(
        Rect.fromPoints(globals.rectSelection[0], globals.rectSelection[1]),
        paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => true;
}
