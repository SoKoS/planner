import 'dart:convert';
import 'dart:io';
import 'package:path_provider/path_provider.dart';

Future saveToFile({List data, String filename}) async {
  Directory appDocDir = await getExternalStorageDirectory();
  String path = appDocDir.path;

  File('$path/$filename').create(recursive: true).then((file) {
    file.writeAsString(json.encode(data));
  });
}

Future loadFromFile(String filename) async {
  String contents;

  Directory appDocDir = await getExternalStorageDirectory();
  String path = appDocDir.path;

  File fileToRead = File('$path/$filename');

  try {
    contents = await fileToRead.readAsString();
  } catch (e) {
    return [];
  }

  List elements = json.decode(contents);

  return elements;
}
