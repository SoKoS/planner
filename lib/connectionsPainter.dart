import 'dart:math';

import 'package:flutter/material.dart';
import 'package:planner/globals.dart' as globals;

class ConnectionsPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint()
      ..color = Colors.white
      ..strokeWidth = 1;

    for (var line in globals.connections) {
      var firstElement = getClosestPoint(line[0], line[1]);
      var secondElement = getClosestPoint(line[1], line[0]);

      canvas.drawLine(firstElement, secondElement, paint);
    }
  }

  Offset getClosestPoint(first, second) {
    Offset firstZeroPoint = Offset(
        globals.elements[first]['coords'][0] * globals.scaleGlobal +
            globals.offsetGlobal.dx,
        globals.elements[first]['coords'][1] * globals.scaleGlobal +
            globals.offsetGlobal.dy);
    Offset secondZeroPoint = Offset(
        globals.elements[second]['coords'][0] * globals.scaleGlobal +
            globals.offsetGlobal.dx,
        globals.elements[second]['coords'][1] * globals.scaleGlobal +
            globals.offsetGlobal.dy);

    Offset top = Offset(
        firstZeroPoint.dx +
            globals.elements[first]['size'][0] / 2 * globals.scaleGlobal,
        firstZeroPoint.dy);
    Offset bottom = Offset(
        firstZeroPoint.dx +
            globals.elements[first]['size'][0] / 2 * globals.scaleGlobal,
        firstZeroPoint.dy +
            globals.elements[first]['size'][1] * globals.scaleGlobal);
    Offset left = Offset(
        firstZeroPoint.dx,
        firstZeroPoint.dy +
            globals.elements[first]['size'][1] / 2 * globals.scaleGlobal);
    Offset right = Offset(
        firstZeroPoint.dx +
            globals.elements[first]['size'][0] * globals.scaleGlobal,
        firstZeroPoint.dy +
            globals.elements[first]['size'][1] / 2 * globals.scaleGlobal);

    Offset middle = Offset(
        secondZeroPoint.dx +
            globals.elements[second]['size'][0] / 2 * globals.scaleGlobal,
        secondZeroPoint.dy +
            globals.elements[second]['size'][1] / 2 * globals.scaleGlobal);

    Offset finalPoint = top;
    double minDist = getDistance(top, middle);

    for (Offset point in [bottom, left, right]) {
      double dist = getDistance(point, middle);
      if (dist < minDist) {
        minDist = dist;
        finalPoint = point;
      }
    }

    return finalPoint;
  }

  static double getDistance(first, second) {
    var dx = second.dx - first.dx;
    var dy = second.dy - first.dy;
    return sqrt(dx * dx + dy * dy);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => true;
}
