import 'dart:developer';
import 'dart:math' as dartMath;

import 'package:flutter/material.dart';
import 'package:planner/connectionsPainter.dart';
import 'package:planner/elements/element.dart';
import 'package:planner/bottomMenu.dart';
import 'package:planner/extrasButton.dart';
import 'package:planner/planListPopup.dart';
import 'package:planner/popupMenu.dart';
import 'package:planner/selectionsPainter.dart';
import 'package:vector_math/vector_math_64.dart' as math;
import 'package:planner/globals.dart' as globals;

class MainPage extends StatefulWidget {
  @override
  MainPageState createState() => MainPageState();
}

class MainPageState extends State<MainPage> {
  Offset _startingFocalPoint;
  Offset focalPoint;
  Offset _previousOffset;
  double _previousZoom;

  @override
  void initState() {
    globals.loadElementsFromFile(fileName: globals.currentPlan).then((_) {
      globals.loadOffsetAndScale(filename: globals.currentPlan).then((_) {
        globals.loadConnections(filename: globals.currentPlan).then((_) {
          setState(() {});
        });
      });
    });

    globals.loadOffsetAndScale(filename: globals.currentPlan);

    globals.loadPlans();

    super.initState();
  }

  void _resetScale() {
    setState(() {
      globals.scaleGlobal = 1.0;
    });
  }

  void _changePlanFromUI() {
    Navigator.of(context).push(PageRouteBuilder(
        opaque: false,
        pageBuilder: (BuildContext context, _, __) {
          return PlanListPopup(planToChangeCallback: _changePlan);
        }));
  }

  void _handleScaleStart(ScaleStartDetails details) {
    setState(() {
      _startingFocalPoint = details.focalPoint;
      focalPoint = _startingFocalPoint;
      _previousOffset = globals.offsetGlobal;
      _previousZoom = globals.scaleGlobal;
    });
  }

  void _handleScaleUpdate(ScaleUpdateDetails details) {
    setState(() {
      globals.scaleGlobal = _previousZoom * details.scale;
      // Ensure that item under the focal point stays in the same place despite zooming
      final Offset normalizedOffset =
          (_startingFocalPoint - _previousOffset) / _previousZoom;

      globals.offsetGlobal =
          details.focalPoint - normalizedOffset * globals.scaleGlobal;

      focalPoint = details.focalPoint + globals.offsetGlobal;
    });
  }

  void _handleScaleEnd(_) {
    globals.saveOffsetAndScale(filename: globals.currentPlan);
  }

  void _onMenuAddElement() {
    Offset addPointScreen = Offset((MediaQuery.of(context).size.width - 50) / 2,
        (MediaQuery.of(context).size.height - 300) / 2);

    Offset addPoint = globals.screenPosToGlobalOffset(addPointScreen);

    //Make it stick to the grid
    addPoint = globals.stickToGrid(addPoint);

    Scaffold.of(context).showBottomSheet(
        (_) => PopupMenu(coords: addPoint, callback: addElement));
  }

  void _onElementChange(int index, List data) {
    globals.saveElementsToFile(fileName: globals.currentPlan);

    setState(() {
      globals.elements[index]['data'] = data;
    });
  }

  void _onElementDelete(int index) {
    if (globals.elements[index]["type"] == "plan") {
      var indexToDelete =
          globals.plans.indexOf(globals.elements[index]["data"][0]);
      if (indexToDelete != -1) {
        globals.plans.removeAt(indexToDelete);
        globals.savePlans();
      }
    }

    globals.elements.removeAt(index);

    globals.removeConnection(index).then((_) {
      setState(() {});
    });

    globals.saveElementsToFile(fileName: globals.currentPlan);
    globals.saveConnections(filename: globals.currentPlan);
  }

  void _changePlan(String planName) {
    globals.saveElementsToFile(fileName: globals.currentPlan);
    globals.saveOffsetAndScale(filename: globals.currentPlan);

    globals.currentPlan = planName;
    globals.loadElementsFromFile(fileName: planName).then((_) {
      globals.loadOffsetAndScale(filename: planName).then((_) {
        globals.loadConnections(filename: planName).then((_) {
          setState(() {});
        });
      });
    });
  }

  void _onResize(int index) {
    globals.saveElementsToFile(fileName: globals.currentPlan);
  }

  void _onLineConnect(int firstElement, int secondElement) {
    globals.connections.add([firstElement, secondElement]);
    globals.saveConnections(filename: globals.currentPlan);

    setState(() {});
  }

  void addElement(String type, Offset coords, List data) {
    var _element = Map();
    _element['type'] = type;
    _element['coords'] = [coords.dx, coords.dy];
    _element['size'] = [-1.0, -1.0]; //default width height
    _element['data'] = data;

    globals.elements.add(_element);

    globals.saveElementsToFile(fileName: globals.currentPlan);

    if (type == "plan") {
      if (globals.plans.indexOf(data[0]) == -1) {
        globals.plans.add(data[0]);
        globals.savePlans();
      }
    }

    setState(() {});
  }

  void refresh() {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      // the root canvas that has as children the main canvas and whatever ui elements i need
      fit: StackFit.expand,
      children: <Widget>[
        GestureDetector(
          behavior: HitTestBehavior.opaque,
          onScaleStart: _handleScaleStart,
          onScaleUpdate: _handleScaleUpdate,
          onScaleEnd: _handleScaleEnd,
          child: Stack(
            // the main canvas
            children: <Widget>[
              //the canvas that draws the line connections
              CustomPaint(
                child: Container(),
                painter: ConnectionsPainter(),
              ),

              //the elements
              for (var i = 0; i < globals.elements.length; i++)
                Transform(
                  transformHitTests: true,
                  transform: Matrix4.compose(
                      math.Vector3(
                          (globals.elements[i]['coords'][0] *
                                  globals.scaleGlobal) +
                              globals.offsetGlobal.dx,
                          (globals.elements[i]['coords'][1] *
                                  globals.scaleGlobal) +
                              globals.offsetGlobal.dy,
                          0),
                      math.Quaternion(0, 0, 0, 0),
                      math.Vector3(
                          globals.scaleGlobal, globals.scaleGlobal, 0)),
                  child: GestureDetector(
                    onPanUpdate: (details) {
                      setState(() {
                        globals.elements[i]['coords'][0] =
                            globals.elements[i]['coords'][0] + details.delta.dx;
                        globals.elements[i]['coords'][1] =
                            globals.elements[i]['coords'][1] + details.delta.dy;
                      });
                    },
                    //When Drag&Drop ends round the last digit to make a grid feel
                    onPanEnd: (details) {
                      setState(() {
                        globals.elements[i]['coords'][0] = globals
                            .stickToGrid(
                              Offset(globals.elements[i]['coords'][0],
                                  globals.elements[i]['coords'][1]),
                            )
                            .dx;
                        globals.elements[i]['coords'][1] = globals
                            .stickToGrid(
                              Offset(globals.elements[i]['coords'][0],
                                  globals.elements[i]['coords'][1]),
                            )
                            .dy;
                      });
                      globals.saveElementsToFile(fileName: globals.currentPlan);
                    },
                    child: ElementType(
                      id: i,
                      onChangeCallback: _onElementChange,
                      onDelete: _onElementDelete,
                      onChangePlan: _changePlan,
                      onResize: _onResize,
                      onLineConnect: _onLineConnect,
                    ),
                  ),
                ),

              if (globals.selectionMode)
                GestureDetector(
                  onPanStart: (panStartDetails) {
                    globals.rectSelection[0] = panStartDetails.globalPosition;
                  },
                  onPanUpdate: (panUpdateDetails) {
                    globals.rectSelection[1] = panUpdateDetails.globalPosition;

                    setState(() {});
                  },
                  onPanEnd: (panEndDetails) {
                    log("rectSel = ${globals.rectSelection}");
                    //TODO
                    // 1. find min point X
                    // 2. get all elements inside the 2 points X
                    // 3. make a menu popup with delete
                    //                             copy
                    //                             move

                    var newPointA = Offset(
                        dartMath.min<num>(globals.rectSelection[0].dx,
                            globals.rectSelection[1].dx),
                        dartMath.min<num>(globals.rectSelection[0].dy,
                            globals.rectSelection[1].dy));

                    var newPointB = Offset(
                        dartMath.max<num>(globals.rectSelection[0].dx,
                            globals.rectSelection[1].dx),
                        dartMath.max<num>(globals.rectSelection[0].dy,
                            globals.rectSelection[1].dy));

                    globals.rectSelection[0] = newPointA;
                    globals.rectSelection[1] = newPointB;

                    newPointA = globals.screenPosToGlobalOffset(newPointA);
                    newPointB = globals.screenPosToGlobalOffset(newPointB);

                    var i = 0;
                    globals.selectedElements = [];
                    for (var element in globals.elements) {
                      if (element['coords'][0] >= newPointA.dx &&
                          element['coords'][1] >= newPointA.dy) {
                        if (element['coords'][0] <= newPointB.dx &&
                            element['coords'][1] <= newPointB.dy) {
                          globals.selectedElements.add(i);
                        }
                      }
                      i++;
                    }
                    log("selected elements = \n${globals.selectedElements}");

                    //TODO menu here
                  },
                  //the canvas that draws the square selection box
                  child: CustomPaint(
                    child: Container(),
                    painter: SelectionsPainter(),
                  ),
                ),
            ],
          ),
        ),

        Positioned(
          bottom: 70.0,
          left: 10.0,
          child: ExtrasButtons(refresh),
        ),

        // Bottom menu
        Positioned(
          bottom: 0.0,
          left: 0.0,
          child: BottomMenu(
            resetScale: _resetScale,
            addElement: _onMenuAddElement,
            onChangePlan: _changePlanFromUI,
          ),
        ),
      ],
    );
  }
}

class MyCustomPainter {}

void main() {
  runApp(MaterialApp(
    theme: ThemeData.dark(),
    home: Scaffold(
      body: MainPage(),
    ),
  ));
}
