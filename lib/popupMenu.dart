import 'package:flutter/material.dart';

class PopupMenu extends StatefulWidget {
  final Offset coords;
  final Function callback;

  const PopupMenu({Key key, @required this.coords, @required this.callback})
      : super(key: key);

  @override
  _PopupMenuState createState() => _PopupMenuState();
}

class _PopupMenuState extends State<PopupMenu> {
  String type = "";
  List data = [];

  String hintText;
  final textEditingController = TextEditingController();

  @override
  void dispose() {
    textEditingController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white12,
      width: double.infinity,
      height: 300,
      child: Column(
        children: <Widget>[
          Container(
            height: 50,
            width: double.infinity,
            child: ListView(
              scrollDirection: Axis.horizontal,
              children: <Widget>[
                FlatButton(
                  onPressed: () {
                    setState(() {
                      type = "noteLabel";
                      data = [];
                    });
                  },
                  child: Icon(Icons.note),
                ),
                FlatButton(
                  onPressed: () {
                    setState(() {
                      type = "task";
                      hintText = "Enter a the task here.";
                      data = [];
                    });
                  },
                  child: Icon(Icons.done_outline),
                ),
                FlatButton(
                  onPressed: () {
                    setState(() {
                      type = "plan";
                      hintText = "Enter the name of the plan here.";
                      data = [];
                    });
                  },
                  child: Icon(Icons.aspect_ratio),
                ),
                FlatButton(
                  onPressed: () {
                    setState(() {
                      type = "shape";
                      if (data.length == 0) {
                        data.add(0);
                      }
                    });
                  },
                  child: Icon(Icons.crop_square),
                ),
              ],
            ),
          ),
          Container(
            color: Colors.white12,
            height: 250,
            width: double.infinity,
            child: Stack(
              children: <Widget>[
                //parameters editor

                if (type == "" || type == "noteLabel")
                  Container(
                    padding: EdgeInsets.all(20),
                    child: TextField(
                      controller: textEditingController,
                      maxLines: 50,
                      decoration:
                          InputDecoration(hintText: "Enter a the note here."),
                    ),
                  ),

                //noteLabel,task and plan
                if (type == "task" || type == "plan")
                  Container(
                    padding: EdgeInsets.all(20),
                    child: TextField(
                      controller: textEditingController,
                      decoration: InputDecoration(
                          hintText: hintText ?? "Enter a the note here."),
                    ),
                  ),

                if (type == "shape")
                  Container(
                    padding: EdgeInsets.all(20),
                    child: Row(
                      children: <Widget>[
                        FlatButton(
                          onPressed: () {
                            data[0] = 0;
                          },
                          child: Text("Circle"),
                        ),
                        FlatButton(
                          onPressed: () {
                            data[0] = 1;
                          },
                          child: Text("Rectangle"),
                        ),
                      ],
                    ),
                  ),

                //done button
                Positioned(
                    right: 5,
                    bottom: 5,
                    child: SizedBox(
                      height: 50,
                      width: 50,
                      child: FlatButton(
                        padding: EdgeInsets.all(0),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30.0)),
                        color: Colors.white12,
                        child: Icon(Icons.done),
                        onPressed: () {
                          //if the user doesn't tap on a type button the type stays empty so fill it with noteLabel
                          if (type == "") {
                            type = "noteLabel";
                          }

                          if (type == "task" || type == "noteLabel") {
                            data.add(textEditingController.text);
                            data.add(false);
                          }

                          if (type == "plan") {
                            data.add(textEditingController.text);
                          }

                          widget.callback(type, widget.coords,
                              data); // passing data to parent
                          Navigator.pop(context);
                        },
                      ),
                    ))
              ],
            ),
          )
        ],
      ),
    );
  }
}
