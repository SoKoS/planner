import 'package:flutter/material.dart';
import 'package:planner/globals.dart' as globals;

class PlanListPopup extends StatelessWidget {
  final Function planToChangeCallback;

  const PlanListPopup({Key key, this.planToChangeCallback}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTapUp: (_) {
        Navigator.pop(context);
      },
      child: Stack(
        children: <Widget>[
          Positioned(
            top: 30,
            bottom: 30,
            left: 20,
            right: 20,
            child: Container(
              color: Colors.grey,
              child: ListView.builder(
                itemCount: globals.plans.length,
                itemBuilder: (BuildContext context, int index) {
                  return new PlanListItem(
                    index: index,
                    name: globals.plans[index],
                    planToChangeCallbackList: planToChangeCallback,
                  );
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class PlanListItem extends StatelessWidget {
  final int index;
  final String name;

  final Function planToChangeCallbackList;

  const PlanListItem({
    Key key,
    this.name,
    this.index,
    this.planToChangeCallbackList,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      child: FlatButton(
        child: Text(globals.plans[index]),
        onPressed: () {
          planToChangeCallbackList(globals.plans[index]);
          Navigator.pop(context);
        },
      ),
    );
  }
}
