library planner.globals;

import 'dart:ui';
import 'package:planner/planFileManager.dart' as planFileManager;

Offset offsetGlobal = Offset.zero;
double scaleGlobal = 1.0;

List connections = [];

bool selectionMode = false;
List rectSelection = [Offset(0, 0), Offset(0, 0)];
List selectedElements = [];

List elements = [];
String currentPlan = "main";
List plans = [];

bool lineDrawingMode = false;
int selectedElementId = -1;

Offset screenPosToGlobalOffset(Offset screenPos) {
  return (-offsetGlobal + screenPos) / scaleGlobal;
}

Offset stickToGrid(Offset point) {
  return Offset(
    ((point.dx / 10).roundToDouble()) * 10,
    ((point.dy / 10).roundToDouble()) * 10,
  );
}

//Offset and Scale
void saveOffsetAndScale({String filename}) {
  planFileManager.saveToFile(
      data: [offsetGlobal.dx, offsetGlobal.dy, scaleGlobal],
      filename: "savedPositions/$filename");
}

Future loadOffsetAndScale({String filename}) async {
  await planFileManager.loadFromFile("savedPositions/$filename").then((value) {
    if (value.length > 0) {
      offsetGlobal = Offset(value[0], value[1]);
      scaleGlobal = value[2];
    }
  });
}

//plans (all of the plan elements names)
void savePlans() {
  planFileManager.saveToFile(data: plans, filename: "plans/plans");
}

Future loadPlans() async {
  await planFileManager.loadFromFile("plans/plans").then((value) {
    if (value.length > 0) {
      plans = value;
    } else {
      plans = ["main"];
    }
  });
}

//Connections
void saveConnections({String filename}) {
  planFileManager.saveToFile(
      data: connections, filename: "connections/$filename");
}

Future loadConnections({String filename}) async {
  await planFileManager.loadFromFile("connections/$filename").then((value) {
    connections = value;
  });
}

Future removeConnection(int id) async {
  List toBeRemoved = [];

  //find everything that connects to the element id
  for (var i = 0; i < connections.length; i++) {
    if (connections[i].indexOf(id) != -1) {
      toBeRemoved.add(i);
    }
  }

  //delete all the found connections
  for (var i = toBeRemoved.length - 1; i >= 0; i--) {
    connections.removeAt(toBeRemoved[i]);
  }

  //after the deletion of an element subtract 1 from everything thats higher that the id
  //so it won't be out of range later
  for (var con in connections) {
    if (con[0] > id) {
      con[0] -= 1;
    }

    if (con[1] > id) {
      con[1] -= 1;
    }
  }
}

//Elements
void saveElementsToFile({String fileName}) {
  planFileManager.saveToFile(data: elements, filename: fileName);
}

Future loadElementsFromFile({String fileName}) async {
  List loadedElements = await planFileManager.loadFromFile(fileName);
  elements = loadedElements;
}
