import 'package:flutter/material.dart';
import 'package:planner/globals.dart' as globals;

class ExtrasButtons extends StatefulWidget {
  final Function refresh;

  ExtrasButtons(this.refresh);

  @override
  _ExtrasButtonsState createState() => _ExtrasButtonsState();
}

class _ExtrasButtonsState extends State<ExtrasButtons> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 60,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          // Selection button
          // Container(
          //   height: 50,
          //   width: 50,
          //   child: FlatButton(
          //     padding: EdgeInsets.all(0),
          //     shape: RoundedRectangleBorder(
          //         borderRadius: BorderRadius.circular(30.0)),
          //     color: globals.selectionMode == true ? Colors.green : Colors.grey,
          //     child: Icon(
          //       Icons.photo_size_select_small,
          //       color: Colors.white,
          //     ),
          //     onPressed: () {
          //       globals.lineDrawingMode = false;
          //       globals.selectedElementId = -1;
          //       globals.selectionMode = !globals.selectionMode;
          //       globals.rectSelection = [Offset(0, 0), Offset(0, 0)];
          //       widget.refresh();
          //     },
          //   ),
          // ),

          // line drawing button
          Container(
            height: 50,
            width: 50,
            child: FlatButton(
              padding: EdgeInsets.all(0),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(30.0)),
              color:
                  globals.lineDrawingMode == true ? Colors.green : Colors.grey,
              child: Icon(
                Icons.edit,
                color: Colors.white,
              ),
              onPressed: () {
                globals.selectionMode = false;
                globals.lineDrawingMode = !globals.lineDrawingMode;
                if (globals.lineDrawingMode == false) {
                  globals.selectedElementId = -1;
                }
                widget.refresh();
              },
            ),
          ),
        ],
      ),
    );
  }
}
