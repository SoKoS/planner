import 'package:flutter/material.dart';
import 'package:planner/globals.dart' as globals;

class PopupMenuEditing extends StatefulWidget {
  final int elementId;
  final Function onDone;

  const PopupMenuEditing({Key key, @required this.elementId, this.onDone})
      : super(key: key);

  @override
  _PopupMenuEditingState createState() => _PopupMenuEditingState();
}

class _PopupMenuEditingState extends State<PopupMenuEditing> {
  final textEditingController = TextEditingController();

  @override
  void initState() {
    textEditingController.text = globals.elements[widget.elementId]['data'][0];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white12,
      width: double.infinity,
      child: Stack(
        children: <Widget>[
          //editing Area
          if (globals.elements[widget.elementId]['type'] != "noteLabel")
            Container(
              padding: EdgeInsets.fromLTRB(20, 20, 130, 20),
              child: TextField(
                controller: textEditingController,
                scrollPadding: EdgeInsets.symmetric(vertical: 20),
                autofocus: true,
              ),
            ),
          if (globals.elements[widget.elementId]['type'] == "noteLabel")
            Container(
              padding: EdgeInsets.fromLTRB(20, 20, 130, 20),
              child: TextField(
                controller: textEditingController,
                scrollPadding: EdgeInsets.symmetric(vertical: 20),
                maxLines: 10,
                autofocus: true,
              ),
            ),

          //done button
          Positioned(
            right: 5,
            bottom: 5,
            child: SizedBox(
              height: 50,
              width: 50,
              child: FlatButton(
                padding: EdgeInsets.all(0),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30.0)),
                color: Colors.white12,
                child: Icon(Icons.done),
                onPressed: () {
                  globals.elements[widget.elementId]['data'][0] =
                      textEditingController.text;
                  widget.onDone();
                  Navigator.pop(context);
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}
