import 'package:flutter/material.dart';
import 'package:planner/globals.dart' as globals;

class ElementMenu extends StatefulWidget {
  final double x;
  final double y;
  final int elementId;
  final Function onEditPressed;
  final Function onDeletePressed;

  const ElementMenu(
      {Key key,
      @required this.x,
      @required this.y,
      @required this.elementId,
      this.onEditPressed,
      this.onDeletePressed})
      : super(key: key);

  @override
  _ElementMenuState createState() => _ElementMenuState();
}

class _ElementMenuState extends State<ElementMenu>
    with TickerProviderStateMixin {
  AnimationController controller;
  Animation<double> animation;

  initState() {
    super.initState();
    controller = AnimationController(
        duration: const Duration(milliseconds: 150), vsync: this);
    animation = CurvedAnimation(parent: controller, curve: Curves.decelerate);

    controller.forward();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTapDown: (_) {
        Navigator.pop(context);
      },
      child: Stack(
        children: <Widget>[
          Positioned(
            right: widget.x,
            bottom: widget.y,
            child: Container(
              height: 50,
              width: 110,
              child: FadeTransition(
                opacity: animation,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    //delete button
                    Container(
                      height: 50,
                      width: 50,
                      child: FlatButton(
                        padding: EdgeInsets.all(0),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30.0)),
                        color: Colors.grey,
                        child: Icon(
                          Icons.delete,
                          color: Colors.white,
                        ),
                        onPressed: () {
                          widget.onDeletePressed();
                          Navigator.pop(context);
                        },
                      ),
                    ),

                    //edit button
                    //only show it if the element is not a shape
                    if (globals.elements[widget.elementId]['type'] != "shape")
                      SizedBox(
                        height: 50,
                        width: 50,
                        child: FlatButton(
                          padding: EdgeInsets.all(0),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(30.0)),
                          color: Colors.grey,
                          child: Icon(
                            Icons.edit,
                            color: Colors.white,
                          ),
                          onPressed: () {
                            widget.onEditPressed();
                            Navigator.pop(context);
                          },
                        ),
                      ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
