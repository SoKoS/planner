import 'package:flutter/material.dart';
import 'package:planner/elements/elementMenu.dart';
import 'package:planner/popupMenuEditing.dart';
import 'package:planner/globals.dart' as globals;

class ElementType extends StatefulWidget {
  final int id;
  final Function onChangeCallback;
  final Function onDelete;
  final Function onChangePlan;
  final Function onResize;
  final Function onLineConnect;

  const ElementType(
      {Key key,
      @required this.id,
      this.onChangeCallback,
      this.onDelete,
      this.onChangePlan,
      this.onResize,
      this.onLineConnect})
      : super(key: key);

  @override
  _ElementTypeState createState() => _ElementTypeState();
}

class _ElementTypeState extends State<ElementType> {
  String text;

  void initState() {
    super.initState();
    WidgetsBinding.instance
        .addPostFrameCallback((_) => onElementCreated(context));
  }

  void onElementCreated(context) {
    if (globals.elements[widget.id]['type'] != "shape") {
      if (globals.elements[widget.id]['size'][0] <= 0) {
        globals.elements[widget.id]['size'][0] =
            (context.size.width / 10.0).ceilToDouble() * 10.0;
        globals.elements[widget.id]['size'][1] =
            (context.size.height / 10.0).ceilToDouble() * 10.0;
      }
    } else {
      if (globals.elements[widget.id]['size'][0] == -1.0 &&
          globals.elements[widget.id]['size'][1] == -1.0) {
        globals.elements[widget.id]['size'][0] = 50.0;
        globals.elements[widget.id]['size'][1] = 50.0;
      }
    }
    setState(() {});
  }

  void _toggleCompletion() {
    setState(() {
      globals.elements[widget.id]['data'][1] =
          !globals.elements[widget.id]['data'][1];
    });
  }

  void _changePlan() {
    widget.onChangePlan(globals.elements[widget.id]['data'][0]);
  }

  void _onResizeShape(LongPressMoveUpdateDetails details) {
    //the current tap mapped to the global map
    Offset pointPosition = globals
        .stickToGrid(globals.screenPosToGlobalOffset(details.globalPosition));

    //width
    globals.elements[widget.id]['size'][0] =
        pointPosition.dx - globals.elements[widget.id]['coords'][0];
    //height
    globals.elements[widget.id]['size'][1] =
        pointPosition.dy - globals.elements[widget.id]['coords'][1];

    setState(() {});
  }

  void _onResizeEnd() {
    widget.onResize(widget.id);
  }

  void _onEdited() {
    Scaffold.of(context).showBottomSheet(
        (_) => PopupMenuEditing(elementId: widget.id, onDone: _onEditingDone));
  }

  void _onEditingDone() {
    globals.elements[widget.id]['size'][0] = -1.0;
    globals.elements[widget.id]['size'][1] = -1.0;

    WidgetsBinding.instance
        .addPostFrameCallback((_) => onElementCreated(context));

    setState(() {});
  }

  void _deleteSelf() {
    widget.onDelete(widget.id);
  }

  void _handleTapUp(_) {
    if (globals.lineDrawingMode == true) {
      //check if something is already tapped to connect to
      if (globals.selectedElementId != -1) {
        if (globals.selectedElementId != widget.id) {
          widget.onLineConnect(globals.selectedElementId, widget.id);
          setState(() {});
        }

        //done so remove the selectedElement
        globals.selectedElementId = -1;
      } else {
        //didn't found an element add its id to the selectedElementId
        globals.selectedElementId = widget.id;
      }
    } else {
      //show the elementMenu
      double width = MediaQuery.of(context).size.width;
      double height = MediaQuery.of(context).size.height;

      //this is from bottom right to top left so the menu scales in the opposite of the element so it displays right
      Offset _showAtPoint = Offset(
          (width -
                  globals.elements[widget.id]['coords'][0] *
                      globals.scaleGlobal) -
              globals.offsetGlobal.dx -
              70,
          (height -
                  globals.elements[widget.id]['coords'][1] *
                      globals.scaleGlobal) -
              globals.offsetGlobal.dy +
              5);

      Navigator.of(context).push(PageRouteBuilder(
          opaque: false,
          pageBuilder: (BuildContext context, _, __) {
            return ElementMenu(
              x: _showAtPoint.dx,
              y: _showAtPoint.dy,
              elementId: widget.id,
              onEditPressed: _onEdited,
              onDeletePressed: _deleteSelf,
            );
          }));
    }
  }

  @override
  Widget build(BuildContext context) {
    if (globals.elements[widget.id]['type'] == "noteLabel") {
      return GestureDetector(
        onTapUp: _handleTapUp,
        onLongPress: () {
          setState(() {
            globals.elements[widget.id]['data'][1] =
                !globals.elements[widget.id]['data'][1];
          });
        },
        child: Container(
          width: globals.elements[widget.id]['size'][0] <= 0
              ? null
              : globals.elements[widget.id]['size'][0],
          height: globals.elements[widget.id]['size'][1] <= 0
              ? null
              : globals.elements[widget.id]['size'][1],
          decoration: globals.elements[widget.id]['data'][1]
              ? null
              : BoxDecoration(border: Border.all(color: Colors.white54)),
          padding: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
          child: Text(
            text ?? globals.elements[widget.id]['data'][0],
            overflow: TextOverflow.ellipsis,
            maxLines: 50,
          ),
        ),
      );
    } else if (globals.elements[widget.id]['type'] == "task") {
      return GestureDetector(
        onTapUp: _handleTapUp,
        onLongPress: _toggleCompletion,
        child: Container(
          width: globals.elements[widget.id]['size'][0] <= 0
              ? null
              : globals.elements[widget.id]['size'][0],
          height: globals.elements[widget.id]['size'][1] <= 0
              ? null
              : globals.elements[widget.id]['size'][1],
          padding: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
          color: globals.elements[widget.id]['data'][1] == false
              ? Colors.white54
              : Colors.green,
          child: Text(text ?? globals.elements[widget.id]['data'][0]),
        ),
      );
    } else if (globals.elements[widget.id]['type'] == "plan") {
      return GestureDetector(
        onTapUp: _handleTapUp,
        onDoubleTap: _changePlan,
        onLongPress: () {},
        child: Container(
          width: globals.elements[widget.id]['size'][0] <= 0
              ? null
              : globals.elements[widget.id]['size'][0],
          height: globals.elements[widget.id]['size'][1] <= 0
              ? null
              : globals.elements[widget.id]['size'][1],
          padding: EdgeInsets.symmetric(vertical: 15, horizontal: 20),
          color: Colors.black,
          child: Text(text ?? globals.elements[widget.id]['data'][0]),
        ),
      );
    } else if (globals.elements[widget.id]['type'] == "shape") {
      return SizedBox(
        width: globals.elements[widget.id]['size'][0] <= 1.0
            ? 10.0
            : globals.elements[widget.id]['size'][0],
        height: globals.elements[widget.id]['size'][1] <= 1.0
            ? 10.0
            : globals.elements[widget.id]['size'][1],
        child: GestureDetector(
          onTapUp: _handleTapUp,
          onLongPressMoveUpdate: _onResizeShape,
          onLongPressUp: _onResizeEnd,
          child: Container(
            decoration: new BoxDecoration(
              color: Colors.white70,
              shape: globals.elements[widget.id]['data'][0] == 0
                  ? BoxShape.circle
                  : BoxShape.rectangle,
            ),
          ),
        ),
      );
    }

    //No Type
    return Container(child: Text("Don't mess with the saved files please. :)"));
  }
}
