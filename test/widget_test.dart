// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

// import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

// import 'package:planner/main.dart';

void main() {
  test("genericTesting", () {
    var elements = [];
    elements.add(Map());
    elements.add(Map());
    elements.add(Map());

    elements[0]["type"] = "noteLabel";
    elements[0]["coords"] = [1, 1];
    elements[0]["size"] = [15, 15];
    elements[0]["data"] = ["label"];

    elements[1]["type"] = "task";
    elements[1]["coords"] = [7, 7];
    elements[1]["size"] = [400, 400];
    elements[1]["data"] = ["TODOS", true];

    elements[2]["type"] = "shape";
    elements[2]["coords"] = [5, 5];
    elements[2]["size"] = [30, 30];
    elements[2]["data"] = ["circle"];

    expect(elements[0]['data'][0], "label");

    expect(elements[1]['data'][1], true);
    expect(elements[1]['coords'], [7, 7]);

    expect(elements[2]['type'], "shape");
  });

  // testWidgets('Counter increments smoke test', (WidgetTester tester) async {
  //   // Build our app and trigger a frame.
  //   await tester.pumpWidget(MyApp());

  //   // Verify that our counter starts at 0.
  //   expect(find.text('0'), findsOneWidget);
  //   expect(find.text('1'), findsNothing);

  //   // Tap the '+' icon and trigger a frame.
  //   await tester.tap(find.byIcon(Icons.add));
  //   await tester.pump();

  //   // Verify that our counter has incremented.
  //   expect(find.text('0'), findsNothing);
  //   expect(find.text('1'), findsOneWidget);
  // });
}
